(ns giant-bomb.events
  (:require [ajax.core :as ajax]
            [re-frame.core :as rf]))

(rf/reg-fx
  :set-hash
  (fn [{:keys [hash]}]
    (set! (.-hash js/location) hash)))

(def set-user-interceptor [rf/trim-v])

(defonce base-uri "https://www.giantbomb.com/api/search?api_key=844da498b2e22f4040040b3573d17999a47aa4b8&format=json&resources=game&")

(rf/reg-event-fx
  :search-games
  set-user-interceptor
  (fn [{:keys [db]} [query]]
    {:db (assoc db :loading? true)
     :http-xhrio {:method :get
                  :uri (str base-uri "field_list=name,image,id,guid,description&query=" query)
                  :timeout 8000
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success [:games-result]
                  :on-failure [:http-failure]}}))

(rf/reg-event-db
  :games-result
  set-user-interceptor
  (fn [db [{:keys [results]}]]
    (assoc db :games results
              :loading? false)))

(rf/reg-event-db
  :http-failure
  (fn [db [result]]
    (assoc db :loading? false)))

(rf/reg-event-db
  :add-to-cart
  (fn [db [_ id]]
    (update-in db [:cart id] #(if % (inc %) 1))))

(rf/reg-event-fx
  :remove-from-cart
  (fn [{:keys [db]} [_ id]]
    (let [updated-cart (update db :cart dissoc id)
          active-page (if (= 0 (count (:cart updated-cart)))
                        :search
                        :cart)]
      {:db updated-cart
       :dispatch [:set-active-page {:page active-page}]})))

(rf/reg-event-fx
  :set-active-page
  (fn [{:keys [db]} [_ {:keys [page id] :as props} ]]
    (let [set-page (assoc db :active-page page)]
      (case page
        :details {:db (assoc set-page :active-game-id id)}
        {:db set-page}))))

(rf/reg-event-fx
  :view-game-details
  (fn [{:keys [db]} [_ id]]
    {:dispatch [:set-active-page {:page :details :id id}]
     :set-hash {:hash (str "/details/" id)}}))

(rf/reg-event-fx
  :checkout
  (fn [{:keys [db]} [_]]
    {:dispatch [:set-active-page {:page :cart}]
     :set-hash {:hash "/cart"}}))

(rf/reg-event-fx
  :home
  (fn [{:keys [db]} [_]]
    {:dispatch [:set-active-page {:page :search}]
     :set-hash {:hash "/search"}}))

(rf/reg-event-fx
  :rent
  (fn [{:keys [db]} [_]]
    {:db (assoc db :cart {})
     :dispatch [:set-active-page {:page :search}]
     :set-hash {:hash "/search"}}))