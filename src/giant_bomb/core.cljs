(ns giant-bomb.core
  (:require-macros [secretary.core :refer [defroute]])
  (:require [re-frame.core :refer [dispatch-sync]]
            [reagent.dom :as dom]
            [giant-bomb.views :as views]
            [secretary.core :as secretary]
            [re-frame.core :refer [dispatch dispatch-sync]]
            [day8.re-frame.http-fx]
            [giant-bomb.db]
            [giant-bomb.events]
            [giant-bomb.subs]
            [goog.events :as events])
  (:import [goog History]
           [goog.history EventType]))


(defn ^:dev/after-load start []
  (dom/render [views/app]
                      (.getElementById js/document "app")))

(defn ^:dev/before-load stop []
  (js/console.log "stop"))


(defn routes []
  (set! (.-hash js/location) "/")      ;; on app startup set location to "/"
  (secretary/set-config! :prefix "#")  ;; and don't forget about "#" prefix
  (defroute "/" [] (dispatch [:set-active-page {:page :search}]))
  (defroute "/cart" [] (dispatch [:set-active-page {:page :cart}]))
  (defroute "/search" [] (dispatch [:set-active-page {:page :search}]))
  (defroute "/details/:id" [id] (dispatch [:set-active-page {:page :details :id id}])))

(def history
  (doto (History.)
    (events/listen EventType.NAVIGATE #(secretary/dispatch! (.-token %)))
    (.setEnabled true)))

(defn ^:export init []
  (dispatch-sync [:init-db])
  (routes)
  (start))