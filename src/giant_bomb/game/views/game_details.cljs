(ns giant-bomb.game.views.game-details
  (:require [re-frame.core :refer [subscribe dispatch]]))

(defn details []
  (let [game @(subscribe [:active-game])]
    [:div {:dangerouslySetInnerHTML {:__html (:description game)}}]))
