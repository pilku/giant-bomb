(ns giant-bomb.game.views.cart-details
  (:require [re-frame.core :refer [subscribe dispatch]]
            ["@material-ui/icons/Delete" :default DeleteIcon]
            ["@material-ui/core" :as mui]))

(defn details []
  (let [rented? (reagent.core/atom false)
        on-rent-complete #(reset! rented? false)]
    (fn []
      (let [cart-items @(subscribe [:cart-items])]
        [:div#cart-items
         [:> mui/List
          (map-indexed (fn [i {:keys [game qty]}]
                         ^{:key i} [:> mui/ListItem
                                    [:div {:style {:display :flex
                                                   :flex-direction :column}}
                                     [:div {:style {:display :flex
                                                    :cursor :pointer}
                                            :on-click #(dispatch [:view-game-details (:guid game)])}
                                      [:> mui/ListItemAvatar
                                       [:> mui/Avatar {:src (-> game :image :thumb_url)}]]
                                      [:> mui/ListItemText {:primary (-> game :name)}]]
                                     [:div {:style {:display :flex}}
                                      [:> mui/ListItemText {:primary (str "QTY " qty)
                                                            :style {:display :flex
                                                                    :justify-content :space-around
                                                                    :margin-left "10px"
                                                                    :align-items :center}}]
                                      [:> mui/IconButton {:on-click (fn [_]
                                                                      (dispatch [:remove-from-cart (:guid game)]))}
                                       [:> DeleteIcon]]]]])
                       cart-items)]
         [:> mui/Snackbar {:open @rented?
                           :on-close on-rent-complete
                           :auto-hide-duration 1000
                           :message "Order placed successfully!"}]
         [:div {:style {:display :flex}}
          [:> mui/Button {:variant "contained" :color :primary
                          :on-click (fn [_]
                                      (reset! rented? true)
                                      (js/setTimeout #(dispatch [:rent]) 1000))
                          :style {:display :flex}}
           "Rent"]]]))))