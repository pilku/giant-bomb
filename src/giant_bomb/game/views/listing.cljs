(ns giant-bomb.game.views.listing
  (:require ["@material-ui/core/List" :default List]
            ["@material-ui/icons/AddShoppingCart" :default AddShoppingCartIcon]
            ["@material-ui/core" :as mui]
            [reagent.core :as reagent]
            [re-frame.core :refer [subscribe dispatch]]))


(defn games-listing []
  (let [added-to-cart (reagent/atom false)
        on-snackbar-close #(reset! added-to-cart false)]
    (fn [games]
      [:div#game-listing
       [:> List
        (map-indexed (fn [i game]
                       ^{:key i} [:> mui/ListItem
                                  [:div {:style {:display :flex}
                                         :on-click #(dispatch [:view-game-details (:guid game)])}
                                   [:> mui/ListItemAvatar
                                    [:> mui/Avatar {:src (-> game :image :thumb_url)}]]
                                   [:> mui/ListItemText {:primary (-> game :name)}]]
                                  [:> mui/ListItemSecondaryAction
                                   [:> mui/IconButton {:edge "end"
                                                       :on-click (fn [_]
                                                                   (reset! added-to-cart true)
                                                                   (dispatch [:add-to-cart (:guid game)]))}
                                    [:> AddShoppingCartIcon]]]])
                     games)]
       [:> mui/Snackbar {:open @added-to-cart
                         :on-close on-snackbar-close
                         :auto-hide-duration 1000
                         :message "Added to cart!"}]])))