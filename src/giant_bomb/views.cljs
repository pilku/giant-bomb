(ns giant-bomb.views
  (:require ["@material-ui/icons/Menu" :default MenuIcon]
            ["@material-ui/core" :as mui]
            ["@material-ui/icons/ShoppingCart" :default ShoppingCartIcon]
            [re-frame.core :refer [subscribe dispatch]]
            [reagent.core :as reagent]
            [giant-bomb.game.views.listing :as game-v]
            [giant-bomb.game.views.game-details :as game-details]
            [giant-bomb.game.views.cart-details :as cart-details]))

(defn search []
  (let [search-field (reagent/atom {:text ""})]
    (fn []
      (let [games @(subscribe [:games])]
        [:div
         [:div {:style {:display :flex
                        :width "100%"
                        :justify-content :space-between}}
          [:> mui/TextField {:id "search-text" :placeholder "Search Games"
                             :variant "outlined" :style {:display :flex
                                                         :flex "10 1 auto"
                                                         :margin-right "20px"}
                             :on-change #(swap! search-field assoc :text (-> % .-target .-value))}]
          [:> mui/Button {:variant "contained" :color :primary
                          :on-click (fn [event]
                                      (.preventDefault event)
                                      (let [search-text (:text @search-field)]
                                        (when (>= (count search-text) 3 )
                                          (dispatch [:search-games search-text]))))
                          :style {:display :flex
                                  :flex "1 0 auto"}}
           "Search"]]
         [game-v/games-listing games]]))))

(defn header []
  (let [no-of-cart-items @(subscribe [:no-of-cart-items])]
    [:div {:style {:display :flex
                   :width "100%"}}
     [:> mui/AppBar {:position "static"}
      [:> mui/Toolbar {:variant "dense"}
       [:> mui/IconButton {:edge "start"}
        [:> MenuIcon]]
       [:> mui/Typography {:variant "h6"
                           :on-click #(dispatch [:home])
                           :style {:flex-grow 1
                                   :cursor :pointer}}
        "Giant Bomb"]
       [:> mui/IconButton {:on-click #(dispatch [:checkout])}
        [:> mui/Badge {:badge-content no-of-cart-items
                       :color :secondary}
         [:> ShoppingCartIcon]]]]]]))

(defn pages [page-name]
  (println "page-name " page-name)
  (case page-name
    :search [search]
    :cart [cart-details/details]
    :details [game-details/details]
    [search]))

(defn app []
  (let [loading @(subscribe [:loading?])
        active-page @(subscribe [:active-page])]
    [:div {:style {:display :flex
                   :width "100%"
                   :flex-direction "column"}}
     [header]
     [:div#container {:style {:margin "20px 100px 0px 100px"}}
      [pages active-page]
      (when loading
        [:> mui/Backdrop {:open true}
         [:> mui/CircularProgress]])]]))
