(ns giant-bomb.subs
  (:require [re-frame.core :refer [reg-sub subscribe]]))

(reg-sub
  :loading?
  (fn [db _]
    (:loading? db)))

(reg-sub
  :games
  (fn [db _]
    (:games db)))

(reg-sub
  :no-of-cart-items
  (fn [db _]
    (reduce + 0 (vals (:cart db)))))

(reg-sub
  :active-page
  (fn [db _]
    (:active-page db)))

(reg-sub
  :cart
  (fn [db _]
    (:cart db)))

(reg-sub
  :active-game
  (fn [db _]
    (let [game-id (:active-game-id db)]
      (first (filter #(= (:guid %) game-id)
                     (:games db))))))

(reg-sub
  :cart-items
  (fn [db _]
    (let [games (:games db)
          game (fn [id]
                 (first (filter #(= (:guid %) id) games)))
          cart (:cart db)]
      (map (fn [[k v]]
             {:game (game k)
              :qty v}) cart))))