(ns giant-bomb.db
  (:require [re-frame.core :as rf]))

(defn init-db []
  {:games []
   :loading? false
   :cart {}
   :active-page ""
   :active-game-id ""})

(rf/reg-event-db
  :init-db
  init-db)
